<!DOCTYPE HTML>
<head>
<html lang="en-US">
<meta charset="UTF-8">
<title>Login</title>

<?php
session_start();
require 'database.php';
echo htmlspecialchars($_SESSION['username']);
if (htmlspecialchars($_SESSION['username']) != NULL && htmlspecialchars($_SERVER['REQUEST_METHOD']) == 'POST'){
	$sess_token = $mysqli->real_escape_string($_SESSION['token']);
	$post_token = $mysqli->real_escape_string($_POST['token']);
	if(!hash_equals($sess_token, $post_token)){
		die("Request forgery detected");
	}
	//$mysqli->query(/* perform transfer */);

	$storytitle = $mysqli->real_escape_string($_POST['storytitle']);
	$storycontent = $mysqli->real_escape_string($_POST['storytext']);
	$storyuri = $mysqli->real_escape_string($_POST['storyuri']);

	$stmt = $mysqli->prepare("insert into stories (username, story_title, story_content, uri) values (?, ?, ?, ?)");
	if(!$stmt){
		die("Story upload failed");
		//printf("Query Prep Failed: %s\n", $mysqli->error);
		//exit;
	}
	$stmt->bind_param('ssss', htmlspecialchars($_SESSION['username']), $storytitle, $storycontent, $storyuri);
	$stmt->execute();
	$stmt->close();
	//header("location:welcome.php");
	$url=htmlspecialchars($_SERVER['HTTP_REFERER']);
	echo "Story upload success!  Redirecting back...";
	header("Refresh: 1; URL=$url");
}
?>
</html>